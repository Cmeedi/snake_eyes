﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake_Eyes
{
    class Die
    {

        Random rand = new Random();

        int die;

        public void roll()
        {
            die = rand.Next(6);
        }

        public int getDie()
        {
            return die;
        }
    }
}

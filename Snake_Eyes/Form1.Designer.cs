﻿namespace Snake_Eyes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RollButton = new System.Windows.Forms.Button();
            this.DieOneLabel = new System.Windows.Forms.Label();
            this.DieTwoLabel = new System.Windows.Forms.Label();
            this.RollsLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // RollButton
            // 
            this.RollButton.Location = new System.Drawing.Point(12, 12);
            this.RollButton.Name = "RollButton";
            this.RollButton.Size = new System.Drawing.Size(130, 115);
            this.RollButton.TabIndex = 0;
            this.RollButton.Text = "Roll";
            this.RollButton.UseVisualStyleBackColor = true;
            this.RollButton.Click += new System.EventHandler(this.RollButton_Click);
            // 
            // DieOneLabel
            // 
            this.DieOneLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DieOneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DieOneLabel.Location = new System.Drawing.Point(169, 23);
            this.DieOneLabel.Name = "DieOneLabel";
            this.DieOneLabel.Size = new System.Drawing.Size(103, 95);
            this.DieOneLabel.TabIndex = 1;
            this.DieOneLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DieTwoLabel
            // 
            this.DieTwoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DieTwoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DieTwoLabel.Location = new System.Drawing.Point(278, 23);
            this.DieTwoLabel.Name = "DieTwoLabel";
            this.DieTwoLabel.Size = new System.Drawing.Size(103, 95);
            this.DieTwoLabel.TabIndex = 2;
            this.DieTwoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RollsLabel
            // 
            this.RollsLabel.Location = new System.Drawing.Point(166, 132);
            this.RollsLabel.Name = "RollsLabel";
            this.RollsLabel.Size = new System.Drawing.Size(100, 23);
            this.RollsLabel.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 158);
            this.Controls.Add(this.RollsLabel);
            this.Controls.Add(this.DieTwoLabel);
            this.Controls.Add(this.DieOneLabel);
            this.Controls.Add(this.RollButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RollButton;
        private System.Windows.Forms.Label DieOneLabel;
        private System.Windows.Forms.Label DieTwoLabel;
        private System.Windows.Forms.Label RollsLabel;
    }
}


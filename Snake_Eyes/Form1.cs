﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake_Eyes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void RollButton_Click(object sender, EventArgs e)
        {
            Die dieOne = new Die();
            Die dieTwo = new Die();
            int count = 0;

            

            while(dieOne.getDie() != 1 && dieTwo.getDie() !=1)
            {
                dieOne.roll();
                dieTwo.roll();
                count++;
                Console.WriteLine(dieOne.getDie());
                Console.WriteLine(dieTwo.getDie());
            }

            
            DieOneLabel.Text = dieOne.getDie().ToString();
            DieTwoLabel.Text = dieTwo.getDie().ToString();
            MessageBox.Show("It took " + count + " Rolls to get Snake Eyes");
        }
    }
}
